<?php
/**
 * @file
 * commons_basic_image_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function commons_basic_image_gallery_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function commons_basic_image_gallery_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function commons_basic_image_gallery_node_info() {
  $items = array(
    'basic_image_gallery' => array(
      'name' => t('Image Gallery'),
      'base' => 'node_content',
      'description' => t('Provides a basic "Image Gallery" content type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

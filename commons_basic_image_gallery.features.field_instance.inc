<?php
/**
 * @file
 * commons_basic_image_gallery.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function commons_basic_image_gallery_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-basic_image_gallery-body'
  $field_instances['node-basic_image_gallery-body'] = array(
    'bundle' => 'basic_image_gallery',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => '1',
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => '1',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => '0',
      ),
    ),
    'display_in_partial_form' => 0,
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => '1',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => '20',
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => '31',
    ),
  );

  // Exported field_instance: 'node-basic_image_gallery-field_basic_image_gallery_images'
  $field_instances['node-basic_image_gallery-field_basic_image_gallery_images'] = array(
    'bundle' => 'basic_image_gallery',
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'lightbox2',
        'settings' => array(),
        'type' => 'lightbox2__lightbox__large__original',
        'weight' => '2',
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'lightbox2',
        'settings' => array(),
        'type' => 'lightbox2__lightbox__large__original',
        'weight' => '2',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'lightbox2',
        'settings' => array(),
        'type' => 'lightbox2__lightshow__medium__original',
        'weight' => '2',
      ),
    ),
    'display_in_partial_form' => 1,
    'entity_type' => 'node',
    'field_name' => 'field_basic_image_gallery_images',
    'label' => 'Images',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => '41',
    ),
  );

  // Exported field_instance: 'node-basic_image_gallery-title_field'
  $field_instances['node-basic_image_gallery-title_field'] = array(
    'bundle' => 'basic_image_gallery',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '4',
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '4',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '3',
      ),
    ),
    'display_in_partial_form' => 1,
    'entity_type' => 'node',
    'field_name' => 'title_field',
    'label' => 'Title',
    'required' => 1,
    'settings' => array(
      'hide_label' => array(
        'entity' => 0,
        'page' => 0,
      ),
      'text_processing' => '0',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => '60',
      ),
      'type' => 'text_textfield',
      'weight' => -5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Images');
  t('Title');

  return $field_instances;
}
